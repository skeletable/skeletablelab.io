((title . "gif-vs-apng"))

# GIF vs APNG

APNGs encoded using gif2apng with default settings.

---

![gif1](https://skeletable.gitlab.io/gif/gif1.gif "Dancing Tewi")

###### 38,3 KB

![apng1](https://skeletable.gitlab.io/apng/apng1.png "Dancing Tewi (APNG variety)")

###### 28,6 KB

*~25% filesize reduction*

---

![gif2](https://skeletable.gitlab.io/gif/gif2.gif "Shooting Tewi")

###### 31,4 KB

![apng2](https://skeletable.gitlab.io/apng/apng2.png "Shooting Tewi (APNG variety)")

###### 26,3 KB

*~16% filesize reduction*

---

![gif3](https://skeletable.gitlab.io/gif/gif3.gif "Dancing Tewi (pani poni dash)")

###### 274,1 KB

![apng3](https://skeletable.gitlab.io/apng/apng3.png "Dancing Tewi (pani poni dash, APNG variety)")

###### 209,9 KB

*~24% filesize reduction*

---

![gif4](https://skeletable.gitlab.io/gif/gif4.gif "RUNNING TEWI")

###### 103,4 KB

![apng4](https://skeletable.gitlab.io/apng/apng4.png "RUNNING TEWI (APNG variety)")

###### 89,4 KB

*~14% filesize reduction*

---

APNG files have significantly smaller file size, without any loss in quality (since PNG is lossless). It's also better than webp, which not only tends to have bigger file size than both APNG and GIF, but also suffers from heavy generation loss.

---

But those are just artwork, what happens when you try to convert actual video? Here is our sample, encoded to mp4:

<dl>
    <video width="480" height="200" autoplay="autoplay" loop="loop" preload="auto">
        <source src="https://skeletable.gitlab.io/mp4/output.mp4" type="video/mp4">
    </video>
</dl>

###### 350 KB

Only 350KB! Not half bad, considering there's a lot of moving objects, which can be a hell to optimize. Let's encode it to APNG:

<pre>
ffmpeg -ss 02:30:49 -i .\Blade.Runner.2049.mkv -plays 0 -t 1 apng5.apng
</pre>

This produces a massive (37,5 MB!) file, so I won't bother with embedding it here.

[Output](https://skeletable.gitlab.io/apng/apng5.png)

Awful, but how does GIF compare? The command below should make a file with the best quality possible when using this format:

<pre>
ffmpeg -ss 02:30:49 -i .\Blade.Runner.2049.mkv -vf "split[s0][s1];[s0]palettegen=stats_mode=single[p];[s1][p]paletteuse=new=1" -loop 0 -t 1 gif5.gif
</pre>

The resulting file is "just" 21,4 MB (43% smaller than APNG), but has a very noticable quality loss.

[Output](https://skeletable.gitlab.io/gif/gif5.gif)

Now what if we encode this GIF to APNG? Once again, gif2apng, default settings.

[Output](https://skeletable.gitlab.io/apng/apng6.png)

The resulting image weighs 19,6 MB (~9% smaller), which is still way too big compared to the mp4 (in fact, 56 times bigger). This proves that converting videos to animated images directly is a bad idea, and you should use mp4 or webm if you want to embed them.